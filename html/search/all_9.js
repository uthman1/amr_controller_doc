var searchData=
[
  ['register_0',['Register',['../struct_register.html',1,'Register'],['../register_8h.html#a3fe1c29a7d473713f7b5ad41d8e4f32d',1,'Register():&#160;register.h']]],
  ['register_2ec_1',['register.c',['../register_8c.html',1,'']]],
  ['register_2eh_2',['register.h',['../register_8h.html',1,'']]],
  ['register_5fadd_3',['register_add',['../register_8c.html#a55b26a90767982276963b272cc0423a8',1,'register_add(Register *reg, uint8_t size):&#160;register.c'],['../register_8h.html#a55b26a90767982276963b272cc0423a8',1,'register_add(Register *reg, uint8_t size):&#160;register.c']]],
  ['register_5faddconfig_4',['register_addConfig',['../register_8c.html#a8bf6a33873008e2dbcb396125ebc1d25',1,'register_addConfig(Config *reg, uint16_t min, uint16_t max, uint16_t def):&#160;register.c'],['../register_8h.html#a8bf6a33873008e2dbcb396125ebc1d25',1,'register_addConfig(Config *reg, uint16_t min, uint16_t max, uint16_t def):&#160;register.c']]],
  ['register_5fend_5',['register_end',['../register_8c.html#a1734ea8cf6309d7e984dc8f8517aa5e8',1,'register_end():&#160;register.c'],['../register_8h.html#a1734ea8cf6309d7e984dc8f8517aa5e8',1,'register_end():&#160;register.c']]],
  ['register_5findex_6',['register_index',['../register_8c.html#aedb64b07a79b2450a7bd2ed3eada36b7',1,'register.c']]],
  ['register_5finit_7',['register_init',['../register_8c.html#a5e3689eea889c8f3efb049c9582a834a',1,'register_init(uint16_t model, uint16_t firmware_version, uint16_t modbus_id):&#160;register.c'],['../register_8h.html#a5e3689eea889c8f3efb049c9582a834a',1,'register_init(uint16_t model, uint16_t firmware_version, uint16_t modbus_id):&#160;register.c']]],
  ['register_5freadwriteend_8',['register_readWriteEnd',['../register_8c.html#a802563997c60c42aec2ad3dc0b3051a7',1,'register_readWriteEnd():&#160;register.c'],['../register_8h.html#a802563997c60c42aec2ad3dc0b3051a7',1,'register_readWriteEnd():&#160;register.c']]],
  ['register_5freadwritestart_9',['register_readWriteStart',['../register_8c.html#a653e816d0ec3bcb383ff830cf68ed4c3',1,'register_readWriteStart():&#160;register.c'],['../register_8h.html#a653e816d0ec3bcb383ff830cf68ed4c3',1,'register_readWriteStart():&#160;register.c']]],
  ['register_5fsize_10',['register_size',['../register_8c.html#ae1c8bf8cf18637713cefc8028241ae26',1,'register.c']]],
  ['register_5fskipaddress_11',['register_skipAddress',['../register_8c.html#a0bbaf46891cdf634a5f81bab5e1d290c',1,'register_skipAddress(uint8_t address):&#160;register.c'],['../register_8h.html#a0bbaf46891cdf634a5f81bab5e1d290c',1,'register_skipAddress(uint8_t address):&#160;register.c']]]
];
